
<?php
  require 'Config/Config.php';
 ?>
<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Luiz Fernando Postingel Quirino">
    <link rel="icon" href="favicon.png">

    <title><?php echo $systemName;?></title>

    <!-- Bootstrap core CSS -->
    <link href="Assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- dynatable css-->
    <link rel="stylesheet" media="all" href="Assets/jspkg-archive/jquery.dynatable.css" />
    <!-- Custom styles for this template -->
    <link href="Assets/css/system.css" rel="stylesheet">
  </head>

  <body>

    <?php require 'View/navbar/index.php'; ?>

    <?php if (isset($_GET['action']) == '') {
              require 'View/container/index.php';
          } else {
              require "View/container/{$_GET['action']}.php";
          } ?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="Assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="Assets/js/vendor/popper.min.js"></script>
    <script src="Assets/js/bootstrap.min.js"></script>
    <script type='text/javascript' src="Assets/jspkg-archive/jquery.dynatable.js"></script>
  </body>
</html>
