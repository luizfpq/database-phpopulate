<main role="main" class="container" >
  <div class="jumbotron">
    <form name="cadastro" method="post" enctype="multipart/form-data">
      <input type="hidden" name="action" value="import">
      <div class="form-group">
        <input type="file" class="form-control-file btn" id="uploadedfile" name="uploadedfile" onchange="form.submit()">
      </div>
  </form>
  </div>
</main>
<?php
/* @todo remover inserção do setor na tabela de serviços
 */
use Control\DB;
use Control\Task;
  if ($_FILES['uploadedfile']['error'] == UPLOAD_ERR_OK               //checks for errors
        && is_uploaded_file($_FILES['uploadedfile']['tmp_name'])) { //checks that file is uploaded


// conectando o banco
  include 'Control/DB.php';
  include 'Control/Task.php';

  $db   = new DB($dsn, $user, $pass);
  $task = new Task();

// carregando o CSV do GLPI

          $file=$_FILES['uploadedfile']['tmp_name'];
          $csv= file_get_contents($file);
          $array = array_map("str_getcsv", explode("\n", $csv));
          $json = json_encode($array);
          //print_r($json);
          $string =  $json; //file_get_contents("teste.json");
          $jsonString = json_decode($string, true);

          $i = 1;
          $count = 0;
          //limpando tabela
          $db->exec('truncate task');
          while (($jsonString[$i][0]) != '') {
            if (strpos($jsonString[$i][0], 'Atribuído') !== false) {
                $jsonArray = explode (';', $jsonString[$i][0]);
                
                $taskName        = str_replace('"', '',$jsonArray[1]);
                $taskClient      = explode(':', str_replace('"', '',$jsonArray[5]));

                $query = "select clientSector from client where clientName like '%{$taskClient[0]}%'";
                //echo $query;
                $taskSector = $db->query($query)->fetch();
                //var_dump($taskSector);
                $jsonArray[0] = str_replace(' ', '', $jsonArray[0]);
                $taskDescription = 'http://suporte.ifsp.edu.br/front/ticket.form.php?id='.$jsonArray[0];
                $query = $task->create_task($taskName, $taskClient[0], $taskSector['clientSector'], $taskDescription);
                $retorno = $db->insertion($query);
                $count++;
                //var_dump($retorno);
                }
              $i++;
            }
            if ($retorno = 'sucesso') {
              echo "<div class='alert alert-success' role='alert'>
                      {$count} chamados importados
                    </div>";
            }
            }
?>
