<?php
/**
 * show navbar status active
 */
 function is_active($link){
   if (isset($_GET['action'])) {
     $action = $_GET['action'];
     echo ($action == $link) ? 'class="nav-item active"' : 'class="nav-item"';
   }
   if ($link == '') {
     echo 'class="nav-item active"';
   }
 }

 ?>
