<?php
require 'functions.php'; ?>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
  <a class="navbar-brand" href="#"><?php echo $systemName; ?></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">
      <li <?php is_active('')?>>
        <a class="nav-link" href="<?php echo $systemURL?>">Home</a>
      </li>
      <li <?php is_active('tableCreator')?>>
        <a class="nav-link" href="?action=tableCreator">Criar tabela</a>
      </li>
      <li <?php is_active('setor')?>>
        <a class="nav-link" href="?action=setor">Setor</a>
      </li>
    </ul>
  </div>
</nav>
