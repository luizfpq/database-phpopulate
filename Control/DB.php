<?php
  /* conexão com o banco via PDO
   */
namespace Control;
use \PDO;
class DB extends PDO
{
  public $dsn;
  public $user;
  public $pass;

  public function __construct($dsn, $user, $pass) {
        parent::__construct($dsn, $user, $pass);
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

  public function insertion($query){
     $this->beginTransaction();
       try {
             $this->exec($query);
         } catch (PDOException $err) {
             $value = 'Erro: ' . $err->getMessage();
             $this->rollBack();
         }
         $value = "sucesso";
     $this->commit();
     return $value;
   }
}
 ?>
